//Exception Handling
package javaSheetDay3;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Day3Ques5 {
	public static void main(String a[])
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try
		{
			System.out.print("Enter value :");
			int numerator = Integer.parseInt(reader.readLine());
			System.out.print("Enter value :");
			int denominator = Integer.parseInt(reader.readLine());
			System.out.println("division = "+(numerator/denominator));
			
			reader.close();
		}
		catch(IOException | ArithmeticException | NumberFormatException exception)
		{
			System.out.println("Exception "+exception+" occurs");
		}
	}
}
