//throw and throws Keyword

package javaSheetDay3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.Exception;

class BelowAgeException extends Exception
{
	BelowAgeException(String msg)
	{
		super(msg);
	}
}
public class Day3Ques6and8 
{
	public static void vote(int age) throws BelowAgeException
	{
		if(age < 18)
			throw new BelowAgeException("You are not eligible");
		else 
			System.out.println("You are eligible");
	}
	
	public static void main(String a[])
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter age:");
		try
		{
			String check = reader.readLine();
			if(check != null)
			{	
				int age = Integer.parseInt(check);
				vote(age);
			}
			reader.close();
		}
		catch(BelowAgeException exception)
		{
			System.out.println(exception);
		}
		catch(NumberFormatException | IOException exc) 
		{
			System.out.println(exc);
		}
	}
}
