package javaSheetDay3;

import java.util.Scanner;

public class Day3Ques11 
{
	public static void main(String a[])
	{
		Scanner scan = new Scanner(System.in);
		String inputString = scan.next();
		scan.close();
		int count=0,j=0;
		for(int i=0;i<inputString.length();i++)
		{
			count=0;
			for(j=0;j<inputString.length();j++)
			{
				if(inputString.charAt(i) == inputString.charAt(j) && i>j)
				{
					break;
				}
				else if(inputString.charAt(i) == inputString.charAt(j))
					count++;
			}
			if(count > 0)
				System.out.println(inputString.charAt(i)+" : "+count);
		}
	}
}
