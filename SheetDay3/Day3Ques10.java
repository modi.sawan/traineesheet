package javaSheetDay3;

import java.util.Scanner;

public class Day3Ques10 
{
	public static void main(String a[])
	{
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter first : ");
		int first = scan.nextInt();
		System.out.print("Enter second : ");
		int second = scan.nextInt();

		System.out.print("Enter operator : ");
		String operator = scan.next();
		
		switch(operator)
		{
			case "+" : System.out.println(first+second);break; 
			case "-" : System.out.println(first-second);break; 
			case "*" : System.out.println(first*second);break; 
			case "/" : System.out.println(first/second);break; 
		}
		
		scan.close();
	}
}
