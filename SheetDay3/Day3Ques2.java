//Final Keyword

package javaSheetDay3;

final class FinalClass 
{
	final double pie = 3.14;
	public void display(double newPieValue)
	{
		//this.pie = newPieValue; 
		System.out.println("Display method of Final class");
	}
}

class DerivedClassOfFinal //extends FinalClass
{
	final public void show()
	{
		System.out.println("Final show method");
	}
}

class DerivedClass extends DerivedClassOfFinal
{
	//public void show()
	{
		//Cannot overide Final Method
	}
}

public class Day3Ques2 
{
	public static void main(String args[])
	{
		FinalClass finalClassObject = new FinalClass();
		finalClassObject.display(3.20);
		DerivedClass derivedObject = new DerivedClass();
		derivedObject.show();
	}
}
