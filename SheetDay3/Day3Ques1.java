//Static block

package javaSheetDay3;

class TestStatic
{
	static 
	{
		System.out.println("Test Static");
	}
}

class TestStaticAgain
{
	static 
	{
		System.out.println("Test Static Again");
	}
}

public class Day3Ques1 
{
	static
	{
		System.out.println("Hello and Welcome");
	}
	Day3Ques1()
	{
		System.out.println("Constructor Called");
	}
	public static void main(String args[])
	{
		new TestStaticAgain(); 
		new TestStatic(); 
		new Day3Ques1();
	}
}
