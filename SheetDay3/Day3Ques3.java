package javaSheetDay3;

enum Day{
	SATURDAY , SUNDAY;
}

public class Day3Ques3 {
	final double pie = 3.14;
	
	enum Season {
		WINTER(10), SUMMER(20), SPRING(70);

		int fav;
		Season(int input) {
			fav = input;
		}
		
		void sayEnum()
		{
			System.out.println("sayEnum");
		}
	}

	public static void main(String args[]) {
		Day3Ques3 objectForCons = new Day3Ques3();
		
		for (Season s : Season.values()) {
			switch(s)
			{
				case WINTER :	System.out.println(s + " " + s.ordinal()+" "+Season.WINTER.fav);break;
				case SUMMER :	System.out.println(s + " " + s.ordinal()+" "+Season.SUMMER.fav);break;
				case SPRING :	System.out.println(s + " " + s.ordinal()+" "+Season.SPRING.fav);break;
				default : System.out.println("Some other season");
			}
		}
		Season.WINTER.sayEnum();
		
		Day dayEnum = Day.SATURDAY;
		System.out.println(dayEnum);
	
		System.out.println("Constant pie = "+ objectForCons.pie);
	}
}
