package javaSheetDay3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Day3Ques7 
{
	public static void main(String a[])
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String randomInput = reader.readLine();          //Checked Exception will occur
			int value = Integer.parseInt(randomInput);
			System.out.println(value/0);					//Unchecked Exception will occur 
			
			reader.close();
		}
		catch(IOException | ArithmeticException exception)
		{
			System.out.println(exception);
		}
	}
}
