//Marker Interface

package javaSheetDay1;

interface Marker{

}
class BaseMarker implements Marker{
	public void say(){
		System.out.println("Hello");
	}
}
class DeriveMarker{
	public void display(){
		System.out.println("World");
	}
}
class Ques10{
	public static void main(String args[])
	{
		BaseMarker baseMarker = new BaseMarker();
		DeriveMarker deriveMarker = new DeriveMarker();
		if(baseMarker instanceof Marker)
			baseMarker.say();
		if(deriveMarker instanceof Marker)
			deriveMarker.display();
	}
}