//SOLID - Interface Segregation

package javaSheetDay1;

interface TwoDShape{
	public void Area();
//	public void Volume();
}
interface ThreeDShape{
	public void Area();
	public void Volume();
}
class Square implements TwoDShape{
	public void Area() {
		System.out.println("Area");
	}
}
class Ques20I
{
	public static void main(String a[])
	{
		Square sqr = new Square();
		sqr.Area();
	}
}
