//Immutable Class

package javaSheetDay1;

final class EmployeeId{
	final String identity;
	EmployeeId(String empId) {
		this.identity = empId;
	}
	public String getId()
	{
		return identity;
	}
}
class Ques9{
	public static void main(String a[])
	{
		EmployeeId emp = new EmployeeId("name000");
		System.out.println(emp.getId());
	}
}
