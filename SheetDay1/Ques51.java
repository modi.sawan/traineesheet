package javaSheetDay1;
import java.util.concurrent.*;
import java.util.*;

class CreateRunnable implements Runnable
{
	public void run()
	{
		System.out.println("Runnable Thread is Running");
	}
}
class CreateCallable implements Callable
{
	public Object call()
	{
		Random obj = new Random();
		Integer randomNum = obj.nextInt(10);
		return randomNum;
	}
}
public class Ques51 
{
	public static void main(String a[])
	{
		CreateRunnable runnable = new CreateRunnable();
		Thread thread = new Thread(runnable); 
		thread.start();
		
		CreateCallable callable = new CreateCallable();
		Integer num = (Integer)callable.call();
		System.out.println(num);
	}
}