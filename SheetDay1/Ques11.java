//Serialization 
//transient keyword

package javaSheetDay1;

import java.io.*;
class SerialInterface implements Serializable{
	String name;
	int id;
	transient String password;
	public void say(){
		System.out.println("Hello");
	}
}

class Ques11{
	public static void main(String args[]) throws Exception
	{
		SerialInterface serialInterface = new SerialInterface();
		serialInterface.say();
		serialInterface.name = "Manoj";
		serialInterface.id = 202;
		serialInterface.password = "0000d";
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("new.txt"));
		os.writeObject(serialInterface);
		
		ObjectInputStream is = new ObjectInputStream(new FileInputStream("new.txt"));
		SerialInterface readingObject = (SerialInterface)is.readObject();
		System.out.print(readingObject.name+" "+readingObject.id+" "+readingObject.password);
	}
}
