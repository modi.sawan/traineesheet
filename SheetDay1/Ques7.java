//Encapsulation
//Ques - 8

package javaSheetDay1;

class Circle{
	private double radius;
	private double area;
	public double getRadius(){
		return radius;
	}
	public double getArea(){
		return area;
	}
	public void setRadius(double r){
		this.radius = r;
	}
	public void setArea(double a){
		this.area = a;
	}
}
class Ques7{
	public static void main(String args[])
	{
		Circle circle = new Circle();
		circle.setRadius(2.50);
		circle.setArea(12.05);
		System.out.println("Radius = "+circle.getRadius());
		System.out.println("Area = "+circle.getArea());
	}
}