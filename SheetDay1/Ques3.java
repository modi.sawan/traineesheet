//Concept of Inheritance

package javaSheetDay1;

class Polymorphism{
	public void add(int a,int b){
		System.out.println(a+b);
	}
}
class PolymorphismCheck extends Polymorphism{
	public void add(float a,float b){
		System.out.println(a+b);
	}
}
class Ques3{
	public static void main(String args[]){
		PolymorphismCheck overload = new PolymorphismCheck();
		overload.add(10,20);
		overload.add(3.5f,5.8f);
	}
}
