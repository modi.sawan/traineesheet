package javaSheetDay1;
import java.util.*;

public class Ques33 {
	public static void main(String ar[])
	{
		ArrayList<Organization> arrayList = new ArrayList<>();
		Organization obj1 = new Organization("product development" , "manoj", "m" , 26, 20000);
		Organization obj2 = new Organization("product development" , "manu", "m" , 21, 25000);
		Organization obj3 = new Organization("sales and marketing" , "raj", "m" , 35, 22000);
		Organization obj4 = new Organization("product development" , "rajesh", "m" , 31 , 22000);
		Organization obj5 = new Organization("sales and marketing" , "rani", "f" , 19 , 28000);
		Organization obj6 = new Organization("sales and marketing" , "suhani", "f" , 22 , 28000);
		Organization obj7 = new Organization("product development" , "usha", "f" , 20 , 22000);
		
		
		arrayList.add(obj1);
		arrayList.add(obj2);
		arrayList.add(obj3);
		arrayList.add(obj4);
		arrayList.add(obj5);
		arrayList.add(obj6);
		arrayList.add(obj7);
		
		int sumOfAge=0;
		for(int i=0;i<arrayList.size();i++)
		{
			Organization detail =  arrayList.get(i);
			sumOfAge += detail.getAge();
		}
		System.out.println("Average Age = "+sumOfAge/arrayList.size());
	}
}

