package javaSheetDay1;
import java.util.*;

public class Ques41 {
	public static void main(String args[])
	{
		ArrayList<Organization> arrayList = new ArrayList<>();
		
		Organization obj1 = new Organization("product development" , "manoj", "m" , 26, 20000, 2014);
		Organization obj2 = new Organization("product development" , "manu", "m" , 21, 25000, 2016);
		Organization obj3 = new Organization("sales and marketing" , "raj", "m" , 35, 22000, 2012);
		Organization obj4 = new Organization("product development" , "rajesh", "m" , 31 , 22000, 2013);
		Organization obj5 = new Organization("sales and marketing" , "rani", "f" , 19 , 28000, 2020);
		Organization obj6 = new Organization("sales and marketing" , "suhani", "f" , 22 , 28000, 2018);
		Organization obj7 = new Organization("product development" , "usha", "f" , 20 , 22000, 2019);
	
		arrayList.add(obj1);
		arrayList.add(obj2);
		arrayList.add(obj3);
		arrayList.add(obj4);
		arrayList.add(obj5);
		arrayList.add(obj6);
		arrayList.add(obj7);
		
		Organization detail;
		System.out.println("PRODUCT DEVELOPMENT DEPARTMENT");
		for(int i=0;i<arrayList.size();i++)
		{
			detail = arrayList.get(i);
			if(detail.getDepartment().equals("product development"))
			{
				System.out.println("Dept  : "+detail.getDepartment());
				System.out.println("Name  : "+detail.getName());
				System.out.println("Gender: "+detail.getGender());
				System.out.println("Age   : "+detail.getAge());
				System.out.println("Salary: "+detail.getSalary());
				System.out.println("Join Year: "+detail.getJoinYear());
				System.out.println();
			}
		}
		System.out.println();
		System.out.println("SALES AND MARKETING DEPARTMENT");
		for(int i=0;i<arrayList.size();i++)
		{
			detail = arrayList.get(i);
			if(detail.getDepartment().equals("sales and marketing"))
			{
				System.out.println("Dept  : "+detail.getDepartment());
				System.out.println("Name  : "+detail.getName());
				System.out.println("Gender: "+detail.getGender());
				System.out.println("Age   : "+detail.getAge());
				System.out.println("Salary: "+detail.getSalary());
				System.out.println("Join Year: "+detail.getJoinYear());
				System.out.println();
			}
		}
	}
}
