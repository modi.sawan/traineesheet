//Cloneable Interface

package javaSheetDay1;
import java.util.*;

class Cloning implements Cloneable{
	int id;
	int salary;
	Cloning(int id,int salary)
	{
		this.id = id;
		this.salary = salary;
	}
	protected Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
}
public class Ques13 {
	public static void main(String ar[]) throws CloneNotSupportedException
	{
		Cloning toBeCloned = new Cloning(101,25000);
		Cloning clone = (Cloning)toBeCloned.clone();
		System.out.println(clone.id);
		System.out.println(clone.salary);
	}
}
