//SOLID - Single-Responsibility

package javaSheetDay1;

class Student{
	int studId;
	String studName;
	Student(int id,String name)
	{
		this.studId = id;
		this.studName = name;
	}
}
class Employee{
	int empId;
	String empName;
	Employee(int id,String name)
	{
		this.empId = id;
		this.empName = name;
	}
}
class Ques20S{
	public static void main(String ar[])
	{
		Student student = new Student(1,"nanu");
		Employee employee = new Employee(1,"raju");
		System.out.println("Employee id : "+employee.empId);
		System.out.println("Student id : "+student.studId);
	}
}
