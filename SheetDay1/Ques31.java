
package javaSheetDay1;
import java.util.*;

public class Ques31 {
	public static void main(String ar[])
	{
		ArrayList<Organization> arrayList = new ArrayList<>();
		Organization obj1 = new Organization("product development" , "manoj", "m" , 26, 20000);
		Organization obj2 = new Organization("product development" , "manu", "m" , 21, 25000);
		Organization obj3 = new Organization("sales and marketing" , "raj", "m" , 35, 22000);
		Organization obj4 = new Organization("product development" , "rajesh", "m" , 31 , 22000);
	
		arrayList.add(obj1);
		arrayList.add(obj2);
		arrayList.add(obj3);
		arrayList.add(obj4);
		
		int maleCount=0,femaleCount=0;
		int maleSalary=0,femaleSalary=0;
		for(int i=0;i<arrayList.size();i++)
		{
			Organization detail =  arrayList.get(i);
			if(detail.getGender().equals("m"))
			{
				maleSalary += detail.getSalary();
				maleCount++;
			}
			else
			{
				femaleSalary += detail.getSalary();
				femaleCount++;
			}
		}
		try {
			System.out.println("No. of Male = "+maleCount);
			System.out.println("Avg Male Salary = "+(maleSalary/maleCount));
			System.out.println("No. of Female = "+femaleCount);
			System.out.println("Avg Feale Salary = "+(femaleSalary/femaleCount));
		}
		catch(Exception e) {
			System.out.println("Number is zero");
		}
	}
}
