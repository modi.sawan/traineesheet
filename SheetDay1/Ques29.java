//file size in bytes, kb, mb
//read into byte array
//compare Files

package javaSheetDay1;

import java.nio.file.*;
import java.io.*;

class Ques29
{
	public static void main(String args[]) throws Exception
	{
		String pathName = "E:\\sheet1\\Demo.txt";
		Path path = Paths.get(pathName);
		System.out.println("In Bytes :"+Files.size(path));
		System.out.println("In KiloBytes :"+Files.size(path)/1024);
		System.out.println("In MegaBytes :"+Files.size(path)/(1024*1024));

		//read into byte array
		File file = new File("E:\\sheet1\\new.txt");
		FileInputStream fileInput = new FileInputStream(file);
		byte byteFile[] = new byte[(int)file.length()];
		byteFile = fileInput.readAllBytes();
		for(byte b : byteFile) {
			System.out.print((char)b);
		}
		
		System.out.println();
		
		//compare Files
		String path2 = "E:\\sheet1\\new.txt";
		int compare = pathName.compareTo(path2);
		if(compare>0)
			System.out.println("Equal");
		else
			System.out.println("Not Equal");
	}
}
