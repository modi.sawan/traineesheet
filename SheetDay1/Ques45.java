package javaSheetDay1;
import java.util.*;

public class Ques45 {
	public static void main(String args[])
	{
		HashMap<Organization,String> mapForOrg = new HashMap<>();
		
		Organization obj1 = new Organization("product development" , "manu", "m" , 21, 25000, 2016);
		Organization obj2 = new Organization("product development" , "manu", "m" , 21, 25000, 2016);
		Organization obj3 = new Organization("sales and marketing" , "raj", "m" , 35, 22000, 2012);
		Organization obj4 = new Organization("product development" , "rajesh", "m" , 31 , 22000, 2013);
		Organization obj5 = new Organization("sales and marketing" , "rani", "f" , 19 , 28000, 2020);
		Organization obj6 = new Organization("sales and marketing" , "rani", "f" , 22 , 28000, 2018);
		Organization obj7 = new Organization("product development" , "usha", "f" , 20 , 22000, 2019);
		
		mapForOrg.put(obj1,"m1");
		mapForOrg.put(obj2,"m2");
		mapForOrg.put(obj3,"m3");
		mapForOrg.put(obj4,"m4");
		mapForOrg.put(obj5,"m3");
		mapForOrg.put(obj6,"m2");
		mapForOrg.put(obj7,"m1");
		
		for(Organization detail: mapForOrg.keySet()) {
			System.out.println(detail.getName());
		}
	}
}