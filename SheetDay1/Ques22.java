//file Exists
//file can Read or Write
//last modified time
//input from Console

package javaSheetDay1;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public class Ques22 {
    public static void main(String args[]) throws Exception
    {
        File file = new File("E:\\sheet1\\Demo.txt");
		if(file.exists())
			System.out.println("File is there");
		else
			System.out.println("Not present");
		System.out.println("Can Write ?? "+file.canWrite());
		System.out.println("Can Read ?? "+file.canRead());
		
		
	    //input from Console
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter content :");
		String content = scan.nextLine();
		FileOutputStream fileOutput = new FileOutputStream(file);
		
		byte b[] = content.getBytes();
		fileOutput.write(b);
		
		FileInputStream fileInput = new FileInputStream(file);
		int i;
		while((i=fileInput.read()) != -1)
		{
			System.out.print((char)i);
		}
		System.out.println();
		
		//last modified time
		Date date = new Date(file.lastModified());
		System.out.println(date);
    } 
}
