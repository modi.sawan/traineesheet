package javaSheetDay1;

class Threading extends Thread{
	public void run(){
		int i=1;
		while(i<10)
		{
			System.out.println(i);
			try{
				Thread.sleep(500);
			}catch(InterruptedException e){
				System.out.println(e);
			}	
			i++;
		}
	}
}
class Test48{
	public static void main(String ar[]) throws Exception
	{
		Threading thread1 = new Threading();
		Threading thread2 = new Threading();
		thread1.start();
		thread1.join(2000);
		thread2.start();
	}
}