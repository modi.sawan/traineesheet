//Polymorphism
//Ques-5 : Overloading and Overriding

package javaSheetDay1;

class Overload{
	public void cal(int a,int b){
		System.out.println(a+b);
	}
	public void reminder(int a,int b) {
		System.out.println(a%b);
	}
}
class InheritOverload extends Overload{
	public void cal(int a,int b,int c){
		System.out.println(a+b+c);
	}
	public void reminder(int a,int b) {
		System.out.println("Overriding "+ a%b);
	}
}
class Ques4{
	public static void main(String args[])
	{
		InheritOverload inheritOverload = new InheritOverload();
		inheritOverload.cal(10,10,10);
		inheritOverload.cal(10,10);
		inheritOverload.reminder(12,10);
	}
}