//SOLID - Open-Closed Principle

package javaSheetDay1;

interface OpenClosePrinciple
{
	void performAction(int a,int b);
}
//OpenForExtension
class Addition implements OpenClosePrinciple
{
	public void performAction(int a,int b)
	{
		System.out.println(a+b);
	}
}
class Multiplication implements OpenClosePrinciple
{
	public void performAction(int a,int b)
	{
		System.out.println(a*b);
	}
}
public class Ques20O 
{
	public static void main(String a[])
	{
		Addition add = new Addition();
		add.performAction(10,20);
		Multiplication multiply = new Multiplication();
		multiply.performAction(10,20);
	}
}
