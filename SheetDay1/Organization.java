package javaSheetDay1;

public class Organization {
	String department = null;
	String name = null;
	String gender = null;
	int age = 0;
	int salary = 0;
	int joinYear = 0;
	public String getDepartment() {
		return department;
	}
	public String getName() {
		return name;
	}
	public String getGender() {
		return gender;
	}
	public int getAge() {
		return age;
	}
	public int getSalary() {
		return salary;
	}
	public int getJoinYear() {
		return joinYear;
	}
	
	Organization(String depart, String name, String gen, int age, int salary)
	{
		this.department = depart;
		this.name = name;
		this.gender = gen;
		this.age = age;
		this.salary = salary;
	}
	Organization(String depart, String name, String gen, int age, int salary,int year)
	{
		this.department = depart;
		this.name = name;
		this.gender = gen;
		this.age = age;
		this.salary = salary;
		this.joinYear = year;
	}
	
	//override
	public int hashCode()
	{
		return age;
	}
	public boolean equals(Object o)
	{
		Organization name = (Organization)o;
		return this.name.equals(name.name);
	}
}

