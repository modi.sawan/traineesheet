//List of all File and Directory
//isFile and isDirectory

package javaSheetDay1;

import java.io.*;

public class Ques21 {
    public static void main(String args[])
    {
        File file = new File("E:\\sheet1");
        File[] fileList = file.listFiles();
        for(File fileName:fileList){
			if(fileName.isFile())
				System.out.println("File : "+fileName.getName());
			else if(fileName.isDirectory())
				System.out.println("Dir : "+fileName.getName());
			else
				System.out.println("Other : "+fileName.getName());
        }
        
        //Extension
        System.out.println("Java Files - ");
        for(File javaFiles: fileList)
        {
        	String fileName = javaFiles.getName();
        	int lastIndex = fileName.lastIndexOf('.');
        	if(fileName.substring(lastIndex+1, fileName.length()).equals("java"))
        	{
        		System.out.println(javaFiles.getName());
        	}
        }
    }
}