//Interface
//AbstractClass
//default keyword

package javaSheetDay1;

interface MakeInterface
{
	public void display();
	public default void hello() 
	{
		System.out.println("Hello");
	}
}
abstract class MakeAbstract
{
	abstract public void show();
	public void say()
	{
		System.out.println("Say");
	}
}
class InheritBoth extends MakeAbstract implements MakeInterface
{
	public void display()
	{
		System.out.println("Display");
	}
	public void show()
	{
		System.out.println("Show");
	}
}
public class Ques14 {
	public static void main(String ar[])
	{
		InheritBoth inherit = new InheritBoth();
		inherit.display();
		inherit.show();
		inherit.say();
		inherit.hello();
	}
}
