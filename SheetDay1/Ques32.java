package javaSheetDay1;
import java.util.*;

public class Ques32 {
	public static void main(String ar[])
	{
		ArrayList<Organization> arrayList = new ArrayList<>();
		Organization obj1 = new Organization("product development" , "manoj", "m" , 26, 20000);
		Organization obj2 = new Organization("product development" , "manu", "m" , 21, 25000);
		Organization obj3 = new Organization("sales and marketing" , "raj", "m" , 35, 22000);
		Organization obj4 = new Organization("product development" , "rajesh", "m" , 31 , 22000);
	
		arrayList.add(obj1);
		arrayList.add(obj2);
		arrayList.add(obj3);
		arrayList.add(obj4);
		
		Set<String> hashSet = new HashSet<>();
		for(int i=0;i<arrayList.size();i++)
		{
			Organization detail = arrayList.get(i);
			hashSet.add(detail.getDepartment());
		}
		for(String str:hashSet)
			System.out.println("Male = "+str);

	}
}

