package javaSheetDay2;

public class StudentInfo {
	private int id;
	private String name;
	private String class_id;
	private int marks;
	private String gender;
	private int age;
	
	StudentInfo(int id,String name,String class_id,int marks,String gender,int age)
	{
		this.id = id;
		this.name = name;
		this.class_id = class_id;
		this.marks = marks;
		this.gender = gender;
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getClassId() {
		return class_id;
	}

	public int getMarks() {
		return marks;
	}

	public String getGender() {
		return gender;
	}

	public int getAge() {
		return age;
	}

	@Override
	public String toString() {
		return "StudentInfo [id=" + id + ", name=" + name + ", class_id=" + class_id + ", marks=" + marks + ", gender="
				+ gender + ", age=" + age + "]";
	}

	
}
