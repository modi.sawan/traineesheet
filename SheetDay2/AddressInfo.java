package javaSheetDay2;

import java.util.stream.Stream;

public class AddressInfo {
	private int id;
	private int pin_code;
	private String city;
	private int student_id;
	
	AddressInfo(int id,int pin_code,String city,int student_id)
	{
		this.id = id;
		this.pin_code = pin_code;
		this.city = city;
		this.student_id = student_id;
	}

	public int getId() {
		return id;
	}

	public int getPincode() {
		return pin_code;
	}

	public String getCity() {
		return city;
	}

	public int getStudent_id() {
		return student_id;
	}

	@Override
	public String toString() {
		return "AddressInfo [id=" + id + ", pin_code=" + pin_code + ", city=" + city + ", student_id=" + student_id
				+ "]";
	}

}
