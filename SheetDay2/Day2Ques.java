package javaSheetDay2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.io.*;
import java.util.Scanner;
import java.util.stream.*;
import java.util.Iterator;
import java.util.List;

public class Day2Ques {
	
	public void getStudentWithPinCode(int pin_code, List<AddressInfo> addressList , List<StudentInfo> studentList)
	{
		
		System.out.println("All students at pincode: "+pin_code);
		Stream<AddressInfo> getListByPincode = addressList.stream().filter(add -> add.getPincode() == pin_code);
		getListByPincode.forEach(add -> studentList.stream().filter(stud -> stud.getId()==add.getStudent_id()).forEach(x -> System.out.println(x)));
		
//		Filter by Gender
		System.out.println("Only male students at pincode: "+pin_code);
		Stream<AddressInfo> getFilterByGender = addressList.stream().filter(add -> add.getPincode() == pin_code);
		getFilterByGender.forEach(add -> studentList.stream().filter(stud -> stud.getId()==add.getStudent_id()).filter(stud -> stud.getGender().equals("M")).forEach(x -> System.out.println(x)));

//		//Filter by Class
		System.out.println("All students of Class C at pincode: "+pin_code);
		Stream<AddressInfo> getFilterBYClass = addressList.stream().filter(add -> add.getPincode() == pin_code);
		getFilterBYClass.forEach(add -> studentList.stream().filter(stud -> stud.getId()==add.getStudent_id()).filter(stud -> stud.getClassId().equals("C")).forEach(x -> System.out.println(x)));
		
//		//Filter by Age < 20
		System.out.println("All students at pincode: "+pin_code+" whose age is less than 20");
		Stream<AddressInfo> getFilterByAge = addressList.stream().filter(add -> add.getPincode() == pin_code);
		getFilterByAge.forEach(add -> studentList.stream().filter(stud -> stud.getId()==add.getStudent_id()).filter(stud -> stud.getAge() < 20).forEach(x -> System.out.println(x)));
		
		//find students with pinCode
		
//		int gotId = -1;
//		for(int i=0;i<addressList.size();i++)
//		{
//			AddressInfo addressDetails = (AddressInfo)addressList.get(i);
//			if(addressDetails.getPincode() == pin_code)
//			{
//				gotId = addressDetails.getStudent_id();
//				if(gotId != -1)
//				{
//					for(Object ob : studentList)
//					{
//						StudentInfo add = (StudentInfo)ob;
//						if(add.getId() == gotId)
//						{
//							System.out.println(add);
//						}
//					}
//				}
//			}
//		}
	}
	
	public void getStudentWithCity(String userCity, List<AddressInfo> addressList , List<StudentInfo> studentList)
	{
		Stream<AddressInfo> getListByCity = addressList.stream().filter(add -> add.getCity().equals(userCity));
		getListByCity.forEach(add -> studentList.stream().filter(stud -> add.getStudent_id() == stud.getId()).forEach(x -> System.out.println(x)));
		
		
//		System.out.println("\nEnter city :");
//		String city = scan.next();
//		for(int i=0;i<addressList.size();i++)
//		{
//			addressDetails = addressList.get(i);
//			if(addressDetails.getCity().equals(city))
//			{
//				int gotId = addressDetails.getStudent_id();
//				System.out.println(studentList.get(gotId-1).getName());
//			}
//		}
	}
	
	public void getAllPassStudents(List<StudentInfo> studentList)
	{
		System.out.println("\nPass Students");
		studentList.stream().filter(pass -> pass.getMarks() > 50).forEach(p -> System.out.println(p));
		
		
//		for(int i=0;i<studentList.size();i++)
//		{
//			studentDetails = studentList.get(i);
//			if(studentDetails.getMarks() > 50)
//			{
//				System.out.println(studentDetails.getName());
//			}
//		}
	}
	
	public void getAllFailStudents(List<StudentInfo> studentList)
	{
		System.out.println("\nFail Students");
		studentList.stream().filter(fail -> fail.getMarks() < 50).forEach(n -> System.out.println(n));
		
		
//		for(int i=0;i<studentList.size();i++)
//		{
//			studentDetails = studentList.get(i);
//			if(studentDetails.getMarks() < 50)
//			{
//				System.out.println(studentDetails.getName());
//			}
//		}
	}
	
	public void getStudentWithClass(String classId , List<StudentInfo> studentList)
	{
		studentList.stream().filter(classs -> classs.getClassId().equals(classId)).forEach(x -> System.out.println(x));
		
		
//		for(int i=0;i<studentList.size();i++)
//		{
//			String clas = studentList.get(i).getClassId();
//			if(clas.equals(classId))
//				System.out.println(studentList.get(i).getName());
//		}
	}
	
	public void getToppers(List<StudentInfo> studentList)
	{
		System.out.println("Toppers Student -");
		Collections.sort(studentList, (x,y)-> y.getMarks()-x.getMarks());
		for(int i=0;i<studentList.size();i++)
		{
			if(i<3)
			{
				switch(i)
				{
					case 0: System.out.println("First  : "+studentList.get(i));break;
					case 1: System.out.println("Second : "+studentList.get(i));break;
					case 2: System.out.println("Third  : "+studentList.get(i));break;
				}
			}
			else if(studentList.get(i).getMarks() > 50)
			{
				System.out.println("Pass   :"+studentList.get(i));
			}
			else
			{
				System.out.println("Fail   :"+studentList.get(i));
			}	
		}
	}
	
	public void getStudentsFailByAge(List<StudentInfo> studentList)
	{
		System.out.println("\nStudents elder than 20 are Fail -");
		studentList.stream().filter(age -> age.getAge() > 20).forEach(x -> System.out.println(x));
	}
	
	public void readCsvFile(String filePath) throws Exception
	{
		System.out.println("\nReading Data from CSV Files");
		File studentCsvFile = new File(filePath);
		Scanner fileReader = new Scanner(studentCsvFile);
		
		while(fileReader.hasNext())
		{
			String[] studentData = fileReader.next().split(",");
			System.out.println("id = "+studentData[0]+" , name = "+studentData[1]+" , section = "+studentData[2]+" , marks = "+studentData[3]+" , gender = "+studentData[4]+" , age = "+studentData[5]);
		}
	}
	
	
	public void getPaginatedList(String userGender , int startOfList, int endOfList) throws Exception
	{
		File studentCsvFile = new File("C:\\Users\\hp\\eclipse-workspace\\javaSheet1\\src\\javaSheetDay2\\student.csv");
		List<StudentInfo> listFromCsv = new ArrayList();
		System.out.println("\n"+ userGender +" students from "+startOfList+" to "+endOfList);
		int count=1;
		Scanner onlyFemaleFileReader = new Scanner(studentCsvFile);
		while(onlyFemaleFileReader.hasNext())
		{
			String[] onlyFemale = onlyFemaleFileReader.next().split(",");
			if(onlyFemale[4].equals(userGender))
			{
				if(count>=startOfList && count<=endOfList)
				{
					System.out.println("id = "+onlyFemale[0]+" , name = "+onlyFemale[1]+" , section = "+onlyFemale[2]);
				}
				count++;
			}
			
			//Adding data of CSV into ArrayList
			listFromCsv.add(new StudentInfo(Integer.parseInt(onlyFemale[0]),onlyFemale[1],onlyFemale[2],Integer.parseInt(onlyFemale[3]),onlyFemale[4],Integer.parseInt(onlyFemale[5])));
		}
//		System.out.println("\nAdding data into list from CSV");
//		listFromCsv.forEach(x -> System.out.println(x));
	}
	
	public void deleteStudentFromLists(int idToDelete , List<AddressInfo> addressList , List<StudentInfo>studentList , List<ClassInfo>classList)
	{
		int countClassA=0,countClassB=0,countClassC=0,countClassD=0;
		boolean bool = false;
		addressList.removeIf(d -> d.getStudent_id() == idToDelete);
		for(StudentInfo o:studentList)
		{
			if(o.getId() == idToDelete)
			{
				bool = studentList.remove(o);
				break;
			}
		}
		for(StudentInfo o:studentList)
		{
			String classIdToCheck = o.getClassId();
			switch(classIdToCheck)
			{
			case "A" : countClassA++;break;
			case "B" : countClassB++;break;
			case "C" : countClassC++;break;
			case "D" : countClassD++;
			}
		}
		if(countClassA == 0)
		{
			classList.removeIf(x -> x.getName().equals("A"));
		}
		if(countClassB == 0)
		{
			classList.removeIf(x -> x.getName().equals("B"));
		}
		if(countClassC == 0)
		{
			classList.removeIf(x -> x.getName().equals("C"));
		}
		if(countClassD == 0)
		{
			classList.removeIf(x -> x.getName().equals("D"));
		}
		if(bool)
			System.out.println("Student id "+idToDelete+" is deleted");
		
		System.out.println("\nStudent List after deleting id "+idToDelete);
		for(StudentInfo o:studentList)
		{
			System.out.println(o);
		}
		System.out.println("\nAddress List after deleting id "+idToDelete);
		addressList.forEach(x -> System.out.println(x));
		System.out.println(classList);
	}
	
	public void getPaginatedListOrderByMarks(String userGender , int startOfList, int endOfList , List<StudentInfo> studentList) throws Exception
	{
		File studentCsvFile = new File("C:\\Users\\hp\\eclipse-workspace\\javaSheet1\\src\\javaSheetDay2\\student.csv");
		List<StudentInfo> listFromCsv = new ArrayList();
		System.out.println("\n"+ userGender +" students from "+startOfList+" to "+endOfList);
		int count=1;
		Scanner onlyFemaleFileReader = new Scanner(studentCsvFile);
		while(onlyFemaleFileReader.hasNext())
		{
			String[] onlyFemale = onlyFemaleFileReader.next().split(",");
			if(onlyFemale[4].equals(userGender))
			{
				if(count>=startOfList && count<=endOfList)
				{
					System.out.println("id = "+onlyFemale[0]+" , name = "+onlyFemale[1]+" , section = "+onlyFemale[2]);
				}
				count++;
				
				//Adding data from Csv into ArrayList
				listFromCsv.add(new StudentInfo(Integer.parseInt(onlyFemale[0]),onlyFemale[1],onlyFemale[2],Integer.parseInt(onlyFemale[3]),onlyFemale[4],Integer.parseInt(onlyFemale[5])));
			}
		}
		System.out.println("\nOrdered by Marks");
		Collections.sort(listFromCsv, (x,y) -> y.getMarks()-x.getMarks());
		System.out.println(listFromCsv);
	}
	
	public void getPaginatedListOrderByName(String userGender , int startOfList, int endOfList , List<StudentInfo> studentList) throws Exception
	{
		File studentCsvFile = new File("C:\\Users\\hp\\eclipse-workspace\\javaSheet1\\src\\javaSheetDay2\\student.csv");
		List<StudentInfo> listFromCsv = new ArrayList();
		System.out.println("\n"+ userGender +" students from "+startOfList+" to "+endOfList);
		int count=1;
		Scanner onlyFemaleFileReader = new Scanner(studentCsvFile);
		while(onlyFemaleFileReader.hasNext())
		{
			String[] onlyFemale = onlyFemaleFileReader.next().split(",");
			if(onlyFemale[4].equals(userGender))
			{
				if(count>=startOfList && count<=endOfList)
				{
					System.out.println("id = "+onlyFemale[0]+" , name = "+onlyFemale[1]+" , section = "+onlyFemale[2]);
				}
				count++;
				
				//Adding data from Csv into ArrayList
				listFromCsv.add(new StudentInfo(Integer.parseInt(onlyFemale[0]),onlyFemale[1],onlyFemale[2],Integer.parseInt(onlyFemale[3]),onlyFemale[4],Integer.parseInt(onlyFemale[5])));
			}
		}
		Collections.sort(listFromCsv, (x,y) -> x.getName().compareTo(y.getName()));
		System.out.println(listFromCsv);
	}
	
	public static void main(String a[]) throws Throwable
	{
		ClassInfo classObj1 = new ClassInfo(1,"A");
		ClassInfo classObj2 = new ClassInfo(2,"B");
		ClassInfo classObj3 = new ClassInfo(3,"C");
		ClassInfo classObj4 = new ClassInfo(4,"D");
		
		StudentInfo studentObj1 = new StudentInfo(1,"stud1","A",88,"F",10);
		StudentInfo studentObj2 = new StudentInfo(2,"stud2","A",70,"F",11);
		StudentInfo studentObj3 = new StudentInfo(3,"stud3","B",88,"M",22);
		StudentInfo studentObj4 = new StudentInfo(4,"stud4","B",55,"M",33);
		StudentInfo studentObj5 = new StudentInfo(5,"stud5","A",30,"F",44);
		StudentInfo studentObj6 = new StudentInfo(6,"stud6","C",30,"F",33);
		StudentInfo studentObj7 = new StudentInfo(7,"stud7","C",10,"F",22);
		StudentInfo studentObj8 = new StudentInfo(8,"stud8","C",0,"M",11);
	
		AddressInfo addressObj1 = new AddressInfo(1,452002,"indore",1);
		AddressInfo addressObj2 = new AddressInfo(2,422002,"delhi",1);
		AddressInfo addressObj3 = new AddressInfo(3,442002,"indore",2);
		AddressInfo addressObj4 = new AddressInfo(4,462002,"delhi",3);
		AddressInfo addressObj5 = new AddressInfo(5,472002,"indore",4);
		AddressInfo addressObj6 = new AddressInfo(6,452002,"indore",5);
		AddressInfo addressObj7 = new AddressInfo(7,452002,"delhi",5);
		AddressInfo addressObj8 = new AddressInfo(8,482002,"mumbai",6);
		AddressInfo addressObj9 = new AddressInfo(9,482002,"bhopal",7);
		AddressInfo addressObj10 = new AddressInfo(10,482002,"indore",8);
	
	
		ArrayList<ClassInfo> classList = new ArrayList<>();
		classList.add(classObj1);
		classList.add(classObj2);
		classList.add(classObj3);
		classList.add(classObj4);
		
		ArrayList<StudentInfo> studentList = new ArrayList<>();
		studentList.add(studentObj1);
		studentList.add(studentObj2);
		studentList.add(studentObj3);
		studentList.add(studentObj4);
		studentList.add(studentObj5);
		studentList.add(studentObj6);
		studentList.add(studentObj7);
		studentList.add(studentObj8);
		Collections.sort(studentList, (x,y)->x.getId()-y.getId());
		
		ArrayList<AddressInfo> addressList = new ArrayList<>();
		addressList.add(addressObj1);
		addressList.add(addressObj2);
		addressList.add(addressObj3);
		addressList.add(addressObj4);
		addressList.add(addressObj5);
		addressList.add(addressObj6);
		addressList.add(addressObj7);
		addressList.add(addressObj8);
		addressList.add(addressObj9);
		addressList.add(addressObj10);
		
		Day2Ques classObject = new Day2Ques();
		
		Scanner scan = new Scanner(System.in);
		
		
		//find students with pincode
		System.out.println("Enter pinCode :");
		int pin_code = scan.nextInt();
		classObject.getStudentWithPinCode(pin_code,addressList, studentList);
		
		
		//find students with city
		System.out.println("\nEnter city :");
		String userCity = scan.next().toLowerCase();
		classObject.getStudentWithCity(userCity,addressList, studentList);
		
		
		//find students who Passed the Exam
		classObject.getAllPassStudents(studentList);
		
		
		//find students who got Failed in the Exam
		classObject.getAllFailStudents(studentList);
	
		
		//Print Toppers
		classObject.getToppers(studentList);
		
		//find students of class X
		System.out.println("\nEnter Class :");
		String classId = scan.next();
		classObject.getStudentWithClass(classId ,studentList);
		
		//Fail Student if Age > 20
		classObject.getStudentsFailByAge(studentList);
		
		
		//Reading CSV file
		classObject.readCsvFile("C:\\Users\\hp\\eclipse-workspace\\javaSheet1\\src\\javaSheetDay2\\student.csv");
		
		
		//Female students from 1-9
		String userGender = "F";
		int startOfList=1;
		int endOfList=9;
		classObject.getPaginatedList(userGender,startOfList,endOfList);
		
		
		//Delete student from Collection
		System.out.print("Enter Id of Student you want to delete -");
		int idToDelete = scan.nextInt();
		classObject.deleteStudentFromLists(idToDelete, addressList, studentList, classList);
	
		
		//Sorting students with Marks
		String userGenderForMarks = "F";
		int startOfListForMarks=1;
		int endOfListForMarks=5;
		classObject.getPaginatedListOrderByMarks(userGenderForMarks,startOfListForMarks,endOfListForMarks , studentList);
		
		
		//Sorting students with Name
		String userGenderForName = "F";
		int startOfListForName=7;
		int endOfListForName=8;
		classObject.getPaginatedListOrderByName(userGenderForName,startOfListForName,endOfListForName , studentList);
		
	}
}























